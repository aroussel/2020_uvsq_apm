#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <inttypes.h>

#define gettime(t) clock_gettime(CLOCK_MONOTONIC_RAW, t)
#define get_sub_seconde(t) (1e-9*(double)t.tv_nsec)
/** return time in second
*/
double get_elapsedtime(void)
{
  struct timespec st;
  int err = gettime(&st);
  if (err !=0) return 0;
  return (double)st.tv_sec + get_sub_seconde(st);
}

void init(double* A, double* B, double* C, int size)
{
  int i = 0;

  srand(2020);

  for(i = 0; i < size; i++)
  {
    A[i] = rand();
    B[i] = rand();
    C[i] = 0.0;
  }
}

void add(double* A, double* B, double* C, int size)
{
  int i = 0;

  for(i = 0; i < size; ++i)
  {
    C[i] = A[i] + B[i];
  }
}

int main(int argc, char** argv){
  int N = 0;

  double *A = NULL;
  double *B = NULL;
  double *C = NULL;

  double t0 = 0., t1 = 0., duration = 0.;

  N = (argc < 2)?1024:atoi(argv[1]);
  fprintf(stdout, "Vectors addition\n  Size: %d\n", N);

  // Memory allocation
  A = (double*) malloc(sizeof(double) * N);
  B = (double*) malloc(sizeof(double) * N);
  C = (double*) malloc(sizeof(double) * N);

  // Value initialization
  init(A, B, C, N);

  // Compute multiplication
  t0 = get_elapsedtime();
  add(A, B, C, N);
  t1 = get_elapsedtime();

  // Pretty print
  duration = (t1 - t0);
  uint64_t nb_op = N;
  fprintf(stdout, "Performance results: \n");
  fprintf(stdout, "  Time: %lf s\n", duration);
  fprintf(stdout, "  MFlops: %.2f\n", (nb_op / duration)*1E-6);

  free(A);
  free(B);
  free(C);

  return 0;
}
